
Content Img looks for images in the node body and stores them in the image field

Content Img was written by Pavel Emelianov (brahman).

Dependencies
------------
 * ImageField

Install
-------

1) Copy the content_img folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).

4) Go to Administer -> Content Img (admin/settings/content_img) and manage 
   the module.
